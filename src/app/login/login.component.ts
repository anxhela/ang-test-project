import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { HotToastService } from '@ngneat/hot-toast'
import { AngularFireAuth } from '@angular/fire/compat/auth'
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  hide: boolean = true;
  constructor(private authService: AuthService, private router: Router, private toast: HotToastService) {
    this.loginForm = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', [Validators.required])
    })
   }

  ngOnInit(): void {}

  onLogin(){
    if(!this.loginForm.valid)
      return;
    const {email, password} = this.loginForm.value;
    this.authService.loginUser(email, password).pipe(
      this.toast.observe({
        success: 'Logged in successfully',
        loading: 'Logging in...',
        error: (error)=> `There was an error: ${error}`
      })
    ).subscribe(()=>{
      this.router.navigate(['tickets'])
    })
  }
  
}
