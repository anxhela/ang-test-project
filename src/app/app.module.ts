import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';

import { environment } from '../environments/environment';

import { LoginComponent } from './login/login.component';

import { MatCardModule } from '@angular/material/card'
import { MatInputModule } from '@angular/material/input'
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatDialogModule } from '@angular/material/dialog'
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule , MAT_DATE_LOCALE } from '@angular/material/core'
import { MatTableModule} from '@angular/material/table'
import {MatPaginatorModule} from '@angular/material/paginator'; 
import {MatSortModule} from '@angular/material/sort'; 
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner'; 

import { ReactiveFormsModule } from '@angular/forms';
import { TicketsComponent } from './tickets/tickets.component';
import { HotToastModule } from '@ngneat/hot-toast';
import { TicketAddComponent } from './ticket-add/ticket-add.component';
import { TicketDetailComponent } from './ticket-detail/ticket-detail.component';
import { TicketChartComponent } from './ticket-chart/ticket-chart.component';
import { TicketsGenerateComponent } from './tickets-generate/tickets-generate.component'
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TicketsComponent,
    TicketAddComponent,
    TicketDetailComponent,
    TicketChartComponent,
    TicketsGenerateComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    HotToastModule.forRoot()
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
