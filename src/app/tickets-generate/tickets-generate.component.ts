import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore' 

@Component({
  selector: 'app-tickets-generate',
  templateUrl: './tickets-generate.component.html',
  styleUrls: ['./tickets-generate.component.css']
})
export class TicketsGenerateComponent implements OnInit {

  constructor(private afStore: AngularFirestore) { }

  ngOnInit(): void {
    const cities = ['Berlin', 'Madrid', 'Rome', 'Paris', 'Budapest', 'Hamburg', 'Barcelona', 'Vienna', 'Munich', 'Milan', 'Tirana']
    const fdates = ['03/01/2022','03/02/2022','03/03/2022','03/04/2022','03/05/2022','03/06/2022','03/07/2022','03/08/2022','03/09/2022','03/10/2022','03/11/2022','03/12/2022','03/13/2022','03/14/2022','03/15/2022','03/16/2022','03/16/2022','03/17/2022','03/18/2022','03/19/2022','03/20/2022','03/21/2022', '03/22/2022','03/23/2022','03/24/2022','03/25/2022','03/26/2022','03/27/2022','03/28/2022','03/29/2022','03/30/2022','03/31/2022']
    const tickets =[];
    for(var i=0; i < cities.length; i++){
        var inbound = cities[i];
        for(var j=0; j < cities.length ; j++){
            if( i === j ) { continue; }
            var outbound = cities[j]
            for(var k=0; k<100; k++){
              const index = Math.floor(Math.random() * (31))
              const index2 = Math.floor(Math.random() * (30 - index + 1)) + index
              var from_date = new Date(fdates[index])
              var to_date =  new Date(fdates[index2])
              var seat_number = Math.floor(Math.random() * (200 - 50 + 1)) + 50
              var inbound_price = Math.floor(Math.random() * (100 - 10 + 1)) + 10
              var outbound_price = Math.floor(Math.random() * (100 - 10 + 1)) + 10
              var price = [inbound_price, outbound_price]
              const t = Math.floor(Math.random() * 2) 
              const types = ['Economy', 'Business'];
              var ticket_type = types[t]
              var id = this.afStore.createId();
              var ticket = {id,inbound,outbound,ticket_type,price,from_date,to_date,seat_number}
              this.afStore.collection('tickets').add(ticket);
              tickets.push(ticket)
            }
            
        }
    }

  console.log(tickets)
  }

}
