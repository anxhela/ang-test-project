import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketsGenerateComponent } from './tickets-generate.component';

describe('TicketsGenerateComponent', () => {
  let component: TicketsGenerateComponent;
  let fixture: ComponentFixture<TicketsGenerateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TicketsGenerateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsGenerateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
