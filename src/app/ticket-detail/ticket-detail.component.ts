import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.css']
})
export class TicketDetailComponent implements OnInit {
  
  ticketData: any
  constructor( private route: ActivatedRoute) {
    const params = this.route.snapshot.queryParamMap.get('state');
    if (params === null) {
      this.ticketData = {}
    } else {
      this.ticketData = JSON.parse(params);
    }
    console.log(this.ticketData)
   }

  ngOnInit(): void {}

}
