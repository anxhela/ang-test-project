export interface Ticket{
    id:string,
    inbound:string,
    outbound:string,
    ticket_type:string,
    ticket_type_id:number,
    price:number,
    from_date:string,
    to_date:string,
    seat_number:number
}    