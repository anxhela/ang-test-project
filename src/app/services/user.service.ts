import { Injectable } from '@angular/core';
import { AuthService} from './auth.service'
import { User } from '../models/user'
import { Observable, of, switchMap } from 'rxjs';
import { AngularFirestore } from '@angular/fire/compat/firestore'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor(private afStore: AngularFirestore, private authService: AuthService) { }

  get loggedUser$(): Observable<User | null>{
    return this.authService.currentUser$.pipe(
      switchMap((user)=>{
        if(!user?.uid){
          return of(null)
        }
        return this.afStore.collection('users').doc(user?.uid).valueChanges() as Observable<User>
      })
    )
  }
  
}
