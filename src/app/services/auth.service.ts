import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth'
import { Observable, from } from 'rxjs'
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser$ = this.afAuth.authState
  userLoggedIn: boolean;
  constructor(private afAuth: AngularFireAuth) {
    this.userLoggedIn = false;
    this.afAuth.onAuthStateChanged((user)=>{
      if(user){
        this.userLoggedIn = true;
      }else{
        this.userLoggedIn = false;
      }
      
    })
  }
  loginUser(email: string, password: string): Observable<any>{
    return from(this.afAuth.signInWithEmailAndPassword(email, password))
  }
  logOut(){
    this.userLoggedIn = false;
    return from(this.afAuth.signOut())
  }
}

