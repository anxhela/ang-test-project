import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentData } from '@angular/fire/compat/firestore'
import { from, Observable, tap } from 'rxjs'
import { AnyCatcher } from 'rxjs/internal/AnyCatcher';
import { Ticket } from '../models/ticket'

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(private afStore: AngularFirestore) { }
  getTicket(){
  }
  addTicket(ticket: Ticket){
    ticket.id = this.afStore.createId();
    return this.afStore.collection('tickets').add(ticket);
  }
  getAllTickets(){
    return this.afStore.collection('tickets').snapshotChanges();
  }
  ticketExists(inbound:any,outbound: any,seat_number: any):boolean{
      // this.afStore.collection('tickets',
      //     ref=>ref.where("inbound", "==", inbound)
      //     .where("outbound", "==", outbound)
      //     .where("seat_number", "==", seat_number)
      //   ).get().subscribe(data=>data.forEach(el=>console.log(el.data())));
      return false;
  }
  
}
