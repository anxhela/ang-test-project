import { Component, OnInit, Input, ViewChild, AfterViewInit} from '@angular/core';
import { Router, NavigationExtras} from '@angular/router';
import { UserService } from '../services/user.service'
import { MatDialog } from '@angular/material/dialog'
import { TicketAddComponent } from '../ticket-add/ticket-add.component'
import { User } from '../models/user'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TicketService } from '../services/ticket.service'

import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@UntilDestroy()
@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.css']
})
export class TicketsComponent implements OnInit {

  loadingTable = true
  user$ = this.userService.loggedUser$
  @Input() currentUser?: User| null;
  displayedColumns: string[] = ['inbound','outbound','ticket_type','price','from_date','to_date','seat_number', 'action']
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private userService: UserService, private ticketService: TicketService, 
    private dialog: MatDialog, private router: Router) { }

  ngOnInit(): void {
    this.userService.loggedUser$
      .pipe(untilDestroyed(this))
      .subscribe((user) => {
        this.currentUser = user
      });
    this.getAllTickets()
  }
  
  openDialog(){
    this.dialog.open(TicketAddComponent,{
      width: '50%'
    })
  }
  getAllTickets(){
    this.ticketService.getAllTickets().subscribe({
        next: (res)=>{
          const  ticketList = res.map((e: any) => {
            const data = e.payload.doc.data();
            data.id = e.payload.doc.id;
            return data
          })
          this.loadingTable = false;
          this.dataSource = new MatTableDataSource(ticketList)
          this.dataSource.paginator = this.paginator
          this.dataSource.sort = this.sort
        },
        error: (err)=>{
          alert('Error while fetching tickets')
        }
    });    
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  showTicket(data:any){
    const queryParams: any = {};
    queryParams.state = JSON.stringify(data);
    const navigationExtras: NavigationExtras = {
      queryParams
    };
    this.router.navigate(['/ticket-detail'], navigationExtras);
  }
}
