import { Component, OnInit } from '@angular/core';
import { EmailValidator, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog'
import { Ticket } from '../models/ticket'
import { TicketService } from '../services/ticket.service';
@Component({
  selector: 'app-ticket-add',
  templateUrl: './ticket-add.component.html',
  styleUrls: ['./ticket-add.component.css']
})
export class TicketAddComponent implements OnInit {

  addTicketForm: FormGroup
  constructor(private ticketService: TicketService, private dialogRef: MatDialogRef<TicketAddComponent>) { 
    this.addTicketForm = new FormGroup({
      'inbound': new FormControl('', Validators.required),
      'outbound': new FormControl('', Validators.required),
      'ticket_type': new FormControl('', Validators.required),
      'price_inbound': new FormControl('', Validators.required),
      'price_outbound': new FormControl('', Validators.required),
      'from_date': new FormControl('', Validators.required),
      'to_date': new FormControl('', Validators.required),
      'seat_number': new FormControl('', [Validators.required])      
    })
  }

  ngOnInit(): void {
  }

  addTicket(){
    if(!this.addTicketForm.valid)
      return;
    if(this.ticketService.ticketExists(this.addTicketForm.value.inbound, this.addTicketForm.value.outbound, this.addTicketForm.value.seat_number)){
      alert('This ticket exists')
      return;
    }
    this.ticketService.addTicket({...this.addTicketForm.value, price: [this.addTicketForm.value.price_inbound, this.addTicketForm.value.price_outbound]})
    this.addTicketForm.reset()
    this.dialogRef.close()
  } 
  



}
