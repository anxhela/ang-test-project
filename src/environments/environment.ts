// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCwPh4jDebsYXoRwJM0evnlsReUVptAnvM",
    authDomain: "ticket-scheduler-c8956.firebaseapp.com",
    projectId: "ticket-scheduler-c8956",
    storageBucket: "ticket-scheduler-c8956.appspot.com",
    messagingSenderId: "288845079092",
    appId: "1:288845079092:web:a6f1bbba726c792fdce061",
    measurementId: "G-8HETHW2R7H"
  }
  // firebase: {
  //   apiKey: "AIzaSyDys34xHAm4JtOK5yFXCwIqHzLZn7xuMbg",
  //   authDomain: "test-32f0e.firebaseapp.com",
  //   projectId: "test-32f0e",
  //   storageBucket: "test-32f0e.appspot.com",
  //   messagingSenderId: "868822689338",
  //   appId: "1:868822689338:web:603eda9587f98aa8593703"
  // }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
